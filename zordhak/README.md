Scripts de zordhak
==================

--------------------------------------------------------------------------------

## salsa_webwml_check_po_and_pot.sh

Ce script permet de vérifier l'état des fichiers **PO** et **POT** du dépôt du site web de Debian (voir liens)

### Fonctionnement

Il effectue les actions suivantes dans l'orde :
+ Mise à jour des fichiers **POT** dans le répertoire `english/po/`,
+ Pour chaque fichier **POT**, vérification de l'existance du fichier **PO** correspondant pour chaque langue,
+ Met à jour les fichiers **PO** à partir des fichiers **POT**,
+ Retourne l'état actuel du dépôt Git.

Note : Il n'effectue pas de `git pull` en début. Cette action est à faire manuellement.

### Variables

Les variables suivantes peuvent êtres transmises :
+ **REPO** : Permet de surcharger l'emplacement du répertoire de travail. Par défaut `/srv/git/webwml`.
+ **DEBUG** : Permet d'effectuer une sortie verbeuse. Par défaut `false`.

### Exemples d'utilisation

Exécution standatd :
```
./salsa_webwml_check_po_and_pot.sh
```

Surcharge de l'emplacement du répertoire de travail :
```
REPO="~/salsa/webwml" ./salsa_webwml_check_po_and_pot.sh
```

Exécution en mode débug :
```
DEBUG=true ./salsa_webwml_check_po_and_pot.sh
```

### Liens

+ [Dépôt du site Web de Debian](https://salsa.debian.org/webmaster-team/webwml)

--------------------------------------------------------------------------------
