#!/bin/bash

################################################################################

# Copyright (C) 2019, Alban Vidal <zordhak@debian.org>

# SPDX-License-Identifier: GPL-3.0-or-later
# License-Filename:        LICENSE_GPL-3.0

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################

# Repository URL of this script:
# https://salsa.debian.org/l10n-fr-team/scripts

################################################################################

REPO=${REPO:="/srv/git/webwml"}

DEBUG=${DEBUG:=false}
DEBUG_SLEEP_TIME="0.5"

################################################################################

if [ ! -d $REPO ] ; then
    echo "ERROR - $REPO is not a directory"
    exit 1
fi

echo
echo "################################################################################"
echo

echo "Update POT files"

POT_UPDATE=false
POT_NUMBER=0

declare -A POT_CHECKSUM_BEFORE

# Populate array with checksum for each POT file
for POT in $REPO/english/po/*.pot ; do
    (( POT_NUMBER ++ ))
    $DEBUG && echo -e "\nDEBUG: POT=$POT"
    # Create checksum to check if file are updated
    CHECKSUM_BEFORE=$(sha1sum $POT)
    $DEBUG && echo -e "DEBUG: $POT - CHECKSUM_BEFORE=$CHECKSUM_BEFORE"
    POT_CHECKSUM_BEFORE[$POT]="$CHECKSUM_BEFORE"
    $DEBUG && sleep $DEBUG_SLEEP_TIME
done

cd $REPO/english/po/
make pot --silent --ignore-errors 2> /dev/null

for POT in $REPO/english/po/*.pot ; do
    $DEBUG && echo -e "\nDEBUG: POT=$POT"

    CHECKSUM_AFTER=$(sha1sum $POT)
    $DEBUG && echo -e "DEBUG: $POT - CHECKSUM_BEFORE=${POT_CHECKSUM_BEFORE[$POT]}"
    $DEBUG && echo -e "DEBUG: $POT -  CHECKSUM_AFTER=$CHECKSUM_AFTER"
    if [[ "${POT_CHECKSUM_BEFORE[$POT]}" != "$CHECKSUM_AFTER" ]] ; then
        echo "- WARN - $POT are updated"
        POT_UPDATE=true
    else
        $DEBUG && echo -e "DEBUG: $POT - Checksum OK"
    fi
    $DEBUG && sleep $DEBUG_SLEEP_TIME
done

echo "$POT_NUMBER POT files"

if $POT_UPDATE ; then
    echo -e "\nWARN - At least one POT file was updated"
else
    echo "OK - All POT files are already up-to-date"
fi

echo -e "\n################################################################################\n"

echo "Checking if PO files exist for each english POT file"

# Create languages list array
declare -a LANG_LIST
LANG_LIST_INDEX=0

# Search availaibles languages
for LANG_DIR in $(find $REPO -mindepth 1 -maxdepth 1 -type d) ; do

    $DEBUG && echo -e "\nDEBUG: LANG_DIR=$LANG_DIR"

    # Extract just dir name
    DIR_NAME=$( basename $LANG_DIR | sed 's/\..*//')
    $DEBUG && echo -e "DEBUG: DIR_NAME=$DIR_NAME"

    # Clean unwanted directories
    case $DIR_NAME in
        ci|english|Perl|locale|"" )
            # Note: "" is used to exclude all hidden directories
            # skip
            $DEBUG && echo -e "DEBUG: $DIR_NAME => SKIP"
            ;;
        * )
            # Add current language in array
            $DEBUG && echo -e "DEBUG: $DIR_NAME => Adding to array LANG_LIST"
            LANG_LIST[$LANG_LIST_INDEX]=$DIR_NAME
            (( LANG_LIST_INDEX ++ ))
            ;;
    esac

    $DEBUG && echo -e "DEBUG: $DIR_NAME - End of loop - sleep $DEBUG_SLEEP_TIME"
    $DEBUG && sleep $DEBUG_SLEEP_TIME

done

echo -e "${#LANG_LIST[@]} langs detected"

$DEBUG && echo -e "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && echo -e "DEBUG: End of loop LANG_DIR"
$DEBUG && echo -e "DEBUG: Number of languages=${#LANG_LIST[@]}"
$DEBUG && echo -e "DEBUG: sleep $DEBUG_SLEEP_TIME"
$DEBUG && echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && sleep $DEBUG_SLEEP_TIME

################################################################################

ALL_PO_EXIST=true

# Now, for each POT files, search if PO file exist for all languages
for POT in $REPO/english/po/*.pot ; do

    $DEBUG && echo -e "\nDEBUG: POT=$POT"

    # Extract filename
    NAME=$( basename $POT | sed 's/\..*//' )
    $DEBUG && echo -e "DEBUG: NAME=$NAME"

    for CURRENT_LANG in ${LANG_LIST[@]} ; do

        $DEBUG && echo -e "DEBUG: CURRENT_LANG=$CURRENT_LANG"

        if ls $REPO/$CURRENT_LANG/po/${NAME}.*.po 1> /dev/null 2>&1 ; then
            $DEBUG && echo -e "DEBUG: $NAME PO file in $CURRENT_LANG already exist"
        else
            echo "- $NAME PO file does not exist in $CURRENT_LANG"
            ALL_PO_EXIST=false
        fi
    done
    
    $DEBUG && echo -e "DEBUG: $POT - End of loop - sleep $DEBUG_SLEEP_TIME"
    $DEBUG && sleep $DEBUG_SLEEP_TIME

done

if $ALL_PO_EXIST ; then
    echo "OK - All PO files exists"
fi

$DEBUG && echo -e "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && echo -e "DEBUG: End of loop POT"
$DEBUG && echo -e "DEBUG: sleep $DEBUG_SLEEP_TIME"
$DEBUG && echo -e "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
$DEBUG && sleep $DEBUG_SLEEP_TIME

echo -e "\n################################################################################\n"

POT_FILE_MISSING=false
PO_UPDATE=false

echo "Update PO files from english POT files"

for PO in $REPO/*/po/*.po ; do
    # Extract name of current PO file
    NAME=$( basename $PO | sed 's/\..*//' )
    $DEBUG && echo -e "\nDEBUG: NAME=$NAME"

    FILE_EN="$REPO/english/po/${NAME}.pot"
    $DEBUG && echo -e "DEBUG: FILE_EN=$FILE_EN"

    # If file exist in english, merge it on current language
    if [ -f $FILE_EN ] ; then
        # Create checksum to check if file are updated
        CHECKSUM_BEFORE=$(sha1sum $PO)
        $DEBUG && echo -e "DEBUG: $PO - CHECKSUM_BEFORE=$CHECKSUM_BEFORE"
        # Merge PO and POT file
        msgmerge --quiet -U $PO $FILE_EN
        CHECKSUM_AFTER=$(sha1sum $PO)
        $DEBUG && echo -e "DEBUG: $PO - CHECKSUM_AFTER=$CHECKSUM_AFTER"
        if [[ "$CHECKSUM_BEFORE" != "$CHECKSUM_AFTER" ]] ; then
            echo "- WARN - $PO are updated"
            PO_UPDATE=true
        else
            $DEBUG && echo -e "DEBUG: $PO - Checksum OK"
        fi
    else
        echo "- WARN - ($PO) pot file $FILE_EN does not exist"
    fi

    $DEBUG && echo -e "DEBUG: $PO - End of loop - sleep $DEBUG_SLEEP_TIME"
    $DEBUG && sleep $DEBUG_SLEEP_TIME

done

if $POT_FILE_MISSING ; then
    echo -e "\nWARN - At least one POT file does not exists"
else
    echo "OK - All POT files exists"
fi

if $PO_UPDATE ; then
    echo -e "\nWARN - At least one PO file was updated"
else
    echo "OK - All PO files has up-to-date"
fi

echo -e "\n################################################################################\n"

echo "Status of git repository"
GIT_STATUS=$(git status --porcelain)
if [ -z "$GIT_STATUS" ] ; then
    echo "OK"
else
    echo -e "$GIT_STATUS"
fi

echo -e "\n################################################################################\n"
