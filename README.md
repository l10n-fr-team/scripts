# Scripts

Scripts des membres de l'équipe pour aider et faciliter les traductions

--------------------------------------------------------------------------------

### Zordhak

[Voir le répertoire spécifique](zordhak)

Liste des scripts :
+ `salsa_webwml_check_po_and_pot.sh` - Vérifications **PO** et **POT** sur le site de Debian
+ `salsa_webwml_check_wml_source.sh` - Vérification de l'existance d'une source WML en anglais

--------------------------------------------------------------------------------

